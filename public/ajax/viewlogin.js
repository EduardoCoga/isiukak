/*function recuperar(){swal({
    title: 'Olvidaste tu contraseña',
    input: 'email',
    inputPlaceholder: 'Email'
    })
}*/
async function recuperar () {
    const {value: email} = await swal({
      title: 'Input email address',
      input: 'email',
      inputPlaceholder: 'Enter your email address'
    })
    
    if (email) {
        console.log(email)
        $.ajax({
            data:{mail:email},
            url:'correocontrasena.php',
            type:'POST',
            beforeSend: function(){
                swal({
                    title: '<strong> Espere un momento.</strong>',
                    type: 'info',
                    html:
                      'Est&aacute;mos confirmando el email...',
                    showCloseButton: false,
                    showCancelButton: false,
                    showConfirmButton:false
                  })
            },
            success:function(response){
                if(response!='error'){
                    swal(
                        '¡Exito!',
                        "<b>Se envio un correo con su nueva contraseña</b>",
                        'success'
                    )
                }else{
                    swal(
                        'Error',
                        "Email inexistente",
                        'error'
                    )
                }
                
            }
        });
    }
    
}
function login(){
    if($('#email').val()!='' && $('#contrasenia').val()!=''){
        $.ajax({
            data:{
                email:$('#email').val(),
                contrasenia:$('#contrasenia').val()
            },
            url:'iniciarSesion.php',
            type:'POST',
            beforeSend: function(){
                swal({
                    title: '<strong> Espere un momento.</strong>',
                    type: 'info',
                    html:
                    'Est&aacute;mos confirmando los datos de acceso...',
                    showCloseButton: false,
                    showCancelButton: false,
                    showConfirmButton:false
                })
            },
            success:function(response){
                if(response=="accesoCorrecto"){
                    window.location.href='index.php'
                }else if (response=="accesoCorrectoAdmin") {
                    window.location.href="views/admin.php"
                }else if (response=="errorUno") {
                    swal(
                    'Error de acceso!',
                    "Datos de acceso uno",
                    'error'
                    )
                    $('#email').val('');$('#contrasenia').val('');
                }else if (response=="hayPedo") {
                    swal(
                    'Error de acceso!',
                    "Datos de acceso hay pedo",
                    'error'
                    )
                    $('#email').val('');$('#contrasenia').val('');
                }else{
                    swal(
                    'Error de acceso!',
                    "Datos de acceso incorrectos",
                    'error'
                    )
                    $('#email').val('');$('#contrasenia').val('');
                }
                
            }
        }); 
    }else{
        swal(
            'Error!',
            "Ingrese los campos solicitados",
            'error'
        )
    }
    
}