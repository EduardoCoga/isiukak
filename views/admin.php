<?php 
include('..\conexionMongo.php');
 ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Admin || ISIUKAK</title>
        <meta
        name='viewport'
        content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
        />
        <link rel="shortcut icon" href="../public/images/comq&1.jpg" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Muli:400,600,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="../public/style/admin.css">
    </head>
    <body>
        <div class="todo">
            <nav>
                <div class="logo">
                <div><img src="../public/images/comq&.jpg" alt="logo"></div>
                </div>
                <ul>
                    <li>Dashboard</li>
                    <!--<li>Components</li>-
                    <li>Widgets<i class="fas fa-chevron-right flecha"></i></li>
                        <ul class="subboton">
                        <li>Desk</li>
                        </ul>
                    <li>Metrics</li>-->
                    <li>Tablas<i class="fas fa-chevron-right flecha"></i></li>
                        <ul class="subboton">
                        <li>Ventas</li>
                        </ul>
                    <!--<li>Timeline</li>-->
                    <li>Salir</li>
                </ul>
            </nav>        

            <main class="panelD">

                <header>
                    <img src="../public/images/man.png" alt="Admin" class="perfil">
                    <img src="img/noti.svg" alt="">
                    <img src="img/chat.svg" alt="">
                </header>

                <h1 class="tituloD">Dashboard</h1>

                <h2>Ventas de los ultimos meses<i class="fas fa-arrow-down"></i></h2>
                <?php
                    $seleccionaColeccionPedido = $cliente -> selectCollection("isiukak","usuario");
                    $query = $seleccionaColeccionPedido -> find();
                    $seleccionaColeccionCompra = $cliente -> selectCollection("isiukak","compra");
                 ?>
                <div class="tabla">
                    <table cellpadding="0" cellspacing="0" >
                    <tr class="negrita">
                        <th>¿Quién compró?</th>
                        <th>Inicial</th>
                        <th>Mediano</th>
                        <th>Premium</th>
                    </tr>
                    <?php 
                        foreach ($query as $pedido) {
                            $id_cliente = $pedido['_id'];
                            //print_r($id_cliente);
                            $query2 = $seleccionaColeccionCompra -> find(
                                ['usuario' => new MongoDB\BSON\ObjectID($id_cliente)]);
                            foreach ($query2 as $compra) {
                     ?>
                    <tr>
                        <td><?php print_r($pedido['nombre_c']); ?></td>
                        <td><?php echo $compra['costo']; ?></td>
                        <td><?php echo $compra['cantidad']; ?></td>
                        <td><?php echo $compra['fecha']; ?></td>
                    </tr>
                    <?php 
                            }
                        }
                     ?>
                    </table>
                </div>

                <div class="formularios">
                    <form class="" action="#" method="post">
                    <h3>Fecha:</h3>
                    <input type="text" name="mes" value="" placeholder="Mes">
                    <br>
                    <h3>Ingresos:</h3>
                    <input type="text" name="inicial" value="" placeholder="Inicial">
                    <input type="text" name="mediano" value="" placeholder="Mediano">
                    <input type="text" name="premium" value="" placeholder="Premium">
                    <br>
                    <input type="submit" name="enviar" value="Enviar" class="botonEnviar">

                    </form>
                </div>


                <h2>Últimas 5 ventas<i class="fas fa-arrow-down"></i></h2>
                <div class="tabla">
                    <table cellpadding="0" cellspacing="0">
                    <tr class="negrita">
                        <th>Paquete</th>
                        <th>Monto</th>
                    </tr>
                    <tr>
                        <th>Inicial</th>
                        <th>$20,000</th>
                    </tr>
                    <tr>
                        <th>Premium</th>
                        <th>$45,000</th>
                    </tr>
                    <tr>
                        <th>Inicial</th>
                        <th>$20,000</th>
                    </tr>
                    <tr>
                        <th>Mediano</th>
                        <th>$32,000</th>
                    </tr>
                    <tr>
                        <th>Premium</th>
                        <th>$45,000</th>
                    </tr>

                    </table>
                </div>

                <div class="grafico">
                    <img src="img/grafico.jpg" alt="">
                </div>
            </main>
        </div>
    </body>
</html>