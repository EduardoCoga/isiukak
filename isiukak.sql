-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-03-2019 a las 17:43:34
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `isiukak`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactanos`
--

CREATE TABLE `contactanos` (
  `idcontacto` int(11) NOT NULL,
  `tipo` varchar(50) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_bin NOT NULL,
  `correo` varchar(50) COLLATE utf8_bin NOT NULL,
  `telefono` int(10) NOT NULL,
  `mensaje` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `contactanos`
--

INSERT INTO `contactanos` (`idcontacto`, `tipo`, `nombre`, `apellidos`, `correo`, `telefono`, `mensaje`) VALUES
(1, '', 's', 's', 's', 2147483647, 'jabjhasdbj'),
(2, 'Informe', 'Eduardo', 'Cordero', 'josecorderoramirez@gmail.com', 2147483647, 'Hola como'),
(3, 'Sugerencia', 'po', 'po', 'po@fmail.com', 2147483647, 'kjsdnfjdsk'),
(4, 'Informe', 'a', 'a', 'a', 2147483647, 'asdnjsandjsanksndakqnqw'),
(5, 'Informe', 'asd', 'asd', 'asd', 987654321, 'lÃ±sdmlfds'),
(6, 'Sugerencia', 'asdkjn', 'kjndsjn', 'kajsndkj', 1234567890, 'askjdnkasnadsnkjas'),
(7, 'Informe', 'manuel', 'cordero', 'jo@fm.com', 1234567890, 'askmdamklsamklas\nalksmldmsal\nasldmsla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_c` text COLLATE utf8_bin NOT NULL,
  `correo` varchar(30) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_c`, `correo`, `password`) VALUES
(0, 'Eduardo', 'josecorderoramirez@gmail.com', '$2y$10$Ww5efiUow76OEpsVjMdd0OWC0brxzzdIpdupDH/Fik0K3YO1oQ.Pq'),
(0, 'a', 'a@q.com', '$2y$10$Ybz2FXU6ifm7zWGb4Zbsju1nFaBeegjT44.wQdcSxFNzCHe/LoL/m');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contactanos`
--
ALTER TABLE `contactanos`
  ADD PRIMARY KEY (`idcontacto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contactanos`
--
ALTER TABLE `contactanos`
  MODIFY `idcontacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
