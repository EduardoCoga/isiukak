    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="public/images/comq&1.jpg" type="image/x-icon">
    <meta name="theme-color" content="#1885ed">
    <title>ISIUKAK || ComQ&</title>
    <link rel="stylesheet" href="public/style/bootstrap.min.css">
    <link rel="stylesheet" href="public/style/style.css">
    <script src="public/style/jquery.min.js"></script>
    <script src="public/style/bootstrap.min.js"></script>
    <script src="public/style/style.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.28.10/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.28.10/sweetalert2.css">
    