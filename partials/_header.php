    <header class="header">
        <div class="container header__container">
            <div class="header__logo">
            <img class="header__img" src="public/images/comq&.png" width="80px"> 
            <h1 class="header__title">
                <!--Com<span class="header__light">Q&</span>-->
            </h1>
            </div> 
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <div class="header__menu">
                <nav id="navbar" class="header__nav collapse">
                    <ul class="nav nav-pills nav-fill">
                        <!--<ul class="header__elenco">-->
                        <li class="header__el"><a href="#acercade" class="header__link">Acerca de </a></li>
                        <li class="header__el"><a href="#servicios" class="header__link">Nuestros servicios.</a></li>
                       <!-- <li class="header__el"><a href="#community" class="header__link">Community</a></li>-->
                        <li class="header__el"><a href="#contactanos" class="header__link">Contáctanos</a></li>
                        <!--<li class="header__el"><a href="#" class="header__link">Vision</a></li>
                        <li class="header__el"><a href="#" class="header__link">News </a></li>-->
                    <?php 
                        session_start();
                        if (isset($_SESSION['usuario'])) {
                            $nombreUsuario = $_SESSION['usuario'];
                            $idCliente = $_SESSION['_id'];
                            $nombre = htmlspecialchars($nombreUsuario);
                            $id_cliente = htmlspecialchars($idCliente);
                    ?>
                    <li class="header__el header__el--blue"><a data-toggle="modal" data-target="#modalseleccion" class="btn btn--white"><?php echo "$nombre"; ?> <span class="glyphicon glyphicon-plus"></span></a></li>
                    <?php        
                        }else{
                    ?>
                    <li class="header__el header__el--blue"><a href="login.php" class="header__link">Iniciar sesión</a></li>
                    <?php        
                        }
                     ?>                 
                    </ul>
                </nav>
            </div>
        </div>
    </header>