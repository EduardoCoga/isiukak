<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300, 400, 500" rel="stylesheet">
    <link rel="stylesheet" href="public/style/login.css">
    <link rel="shortcut icon" href="public/images/comq&1.jpg" type="image/x-icon">
    <script src="public/style/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.28.10/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.28.10/sweetalert2.css">
    <script src="public/sweetalert2.all.min.js"></script>
    <title>ISIUKAK || ComQ&</title>
</head>
    <body>
        <!--
    This was created based on the Dribble shot by Deepak Yadav that you can find at https://goo.gl/XRALsw
    I am @hurickkrugner on Twitter or @hk95 on GitHub. Feel free to message me anytime.
    -->

        <section class="user">
        <div class="user_options-container">
            <div class="user_options-text">
            <div class="user_options-unregistered">
                <h2 class="user_unregistered-title">¿No estás registrado aún?</h2>
                <p class="user_unregistered-text">¡Hazlo ahora!</p>
                <button class="user_unregistered-signup" id="signup-button">Regístrate</button>
            </div>

            <div class="user_options-registered">
                <h2 class="user_registered-title">¿Ya tienes una cuenta?</h2>
                <p class="user_registered-text">Inicia sesión.</p>
                <button class="user_registered-login" id="login-button">Iniciar sesión</button>
            </div>
            </div>
            
            <div class="user_options-forms" id="user_options-forms">
            <div class="user_forms-login">
                <h2 class="forms_title">Inicio de sesión</h2>
                <div class="forms_form">
                    <fieldset class="forms_fieldset">
                        <div class="forms_field">
                            <input type="email" name="email" id="email" placeholder="Email" class="forms_field-input" autofocus />
                        </div>
                        <div class="forms_field">
                            <input type="password" name="contrasenia" id="contrasenia" placeholder="Contraseña" class="forms_field-input"  />
                        </div>
                    </fieldset>
                    <div class="forms_buttons">
                        <button type="button" class="forms_buttons-forgot" onclick="recuperar()">¿Olvidaste tu contraseña?</button>
                        <input type="submit" onclick="login()" name="iniciarsesion" value="Iniciar sesión" class="forms_buttons-action">
                    </div>
                </div>
            </div>
            <div class="user_forms-signup">
                <h2 class="forms_title">Regístrate</h2>
                <form class="forms_form" method="post" action="crearUsuario.php">
                    <fieldset class="forms_fieldset">
                        <div class="forms_field">
                            <input type="text" name="fullname" placeholder="Nombre Completo (Nombre y Apellido)" class="forms_field-input" required />
                        </div>
                        <div class="forms_field">
                            <input type="email" name="emailregistro" placeholder="email" class="forms_field-input" required />
                        </div>
                        <div class="forms_field">
                            <input type="password" name="password" placeholder="Contraseña" class="forms_field-input" required />
                        </div>
                    </fieldset>
                    <div class="forms_buttons">
                        <button  class="forms_buttons-action">Registrarse</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
        </section>
    </body>
    <script src="public/ajax/viewlogin.js"></script>
    <script src="public/style/login.js"></script>
</html>