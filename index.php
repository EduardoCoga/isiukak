<?php 
  include("conexionMongo.php");
 ?>
<html>
  <style>  
        @media screen and (max-width: 720px) {
            iframe {
              display:none;
            }
            .erroranchura{
              display: block;
            }
        }
        @media screen and (min-width: 721px) {
            iframe {
              display:block;
            }
            .erroranchura{
              display: none;
            }
        }
  </style>
  <head>
    <?php
    //verificarExiste();
    include("partials/_head.php");
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <!-- comienza header -->
      <?php
        include("partials/_header.php");
      ?> 
    <!-- termina header -->
    <div class="sect sect--padding-top">
      <div class="container"> 
        <div class="row">
          <div class="col-md-12 site">
              <img class="site__img" src="public/images/isiukak.png">
              <h1 class="site__title text-primary">Rápido, seguro y eficiente, como usted lo merece.</h1>
              <!--<h2 class="site__subtitle">Vote for Block Producer</h2>
              <div class="site__box-link">
                <a class="btn btn--width" href="">Vote</a>
                <a class="btn btn--revert btn--width" href="">Telegram</a>
              </div>-->            
          </div>
        </div>
      </div>
    </div>

    <div class="sect sect--padding-bottom" id="acercade">
      <div class="container">
        <div class="row">
          <h1 class="row__title text-primary">
            <b>ComQ&</b>
          </h1>
          <h2 class="row__sub">Acerca de nosotros</h2>
        </div>
        <div class="row row--margin">
          <div class="col-md-1"></div>
          <div class="col-md-5">
            <h2 class="row__title">
              Quiénes somos...
            </h2>
            <h3 align="justify" class="text-muted">
              Somos una empresa de TI encargada en el desarrollo de 
              nuevas tecnolog&iacute;as para la soluci&oacute;n de problemas cotidianos 
              en las localidades de los ciudadanos.
            </h3>
          </div>
          <div class="col-md-6">
            <h2 class="row__title">
              Misi&oacute;n y Visi&oacute;n.
            </h2>
            <h3 align="justify" class="text-muted">
              Implementar las nuevas tecnolog&iacute;as en el mayor 
              n&uacute;mero posible de localidades de ciudadanos, 
              convirti&eacute;ndonos as&iacute; en un impulsor de “el internet de las cosas” 
              en M&eacute;xico, para posterior a ello poder exportar nuestros productos 
              a pa&iacute;ses en donde no se cuente con dichas tecnolog&iacute;as a un bajo precio.
            </h3>
          </div>   
          <div class="col-md-1"></div>
        </div>
      </div>
    </div>


    <div class="sect sect--padding-bottom" id="servicios">
      <div class="container">
        <div class="row row--center">
          <h1 class="row__title">
            Nuestros Servicios 
          </h1>
          <h2 class="row__sub">¿Qu&eacute; nos hace los mejores?</h2>
        </div>
        <div class="row row--center row--margin">
          <div class="col-md-4 col-sm-4 price-box price-box--purple">
            <div class="price-box__wrap">
              <div class="price-box__img"></div>
              <h1 class="price-box__title">
                Inicial
              </h1>
              <p class="price-box__people">
                Seguridad y control
              </p>
              <h2 class="price-box__discount">
                <span class="price-box__dollar">$</span>20,000<span class="price-box__discount--light"></span>
              </h2>
              <h3 class="price-box__price">
                Módulo de seguridad desde el cual se puede monitorear ingresos y egresos a un establecimiento específico.
              </h3>
              <p class="price-box__feat">
                Contiene:
              </p>
              <ul class="price-box__list">
                <li class="price-box__list-el">Monitoreo mediante cámaras</li>
                <li class="price-box__list-el">Ingreso de contraseña mediante clave númerica</li>
                <li class="price-box__list-el">Aviso al celular cuando alguien ingrese al establecimiento (mensaje de texto)</li>
                <!--<li class="price-box__list-el">Q4 2018 - Additional Node in South America</li>-->
              </ul>
              <div class="price-box__btn">
                <?php 
                  if (isset($_SESSION['usuario'])) {
                ?>
                  <a class="btn btn--purple btn--width" data-toggle="modal" data-target="#modalpedido" onclick="insertid(1)">Contrata ya</a>
                <?php 
                  }else{                  
                ?>
                  <a class="btn btn--purple btn--width" data-toggle="modal" data-target="#modalinformativa">Contrata ya</a>                  
                <?php 
                  }
                ?>
              </div>
            </div>
          </div>
          <!-- second -->
          <div class="col-md-4 col-sm-4 price-box price-box--violet">
            <div class="price-box__wrap">
              <div class="price-box__img"></div>
              <h1 class="price-box__title">
                Mediano
              </h1>
              <p class="price-box__people">
                Cocina
              </p>
              <h2 class="price-box__discount">
              <span class="price-box__dollar">$</span>32,000<span class="price-box__discount--light"></span>
              </h2>
              <h3 class="price-box__price">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </h3>
              <p class="price-box__feat">
                Contiene:
              </p>
              <ul class="price-box__list">
                <li class="price-box__list-el">Módulo de seguridad.</li>
                <li class="price-box__list-el">Módulo de cocina</li>
               <!-- <li class="price-box__list-el">Firewall</li>
                <li class="price-box__list-el"> 1GB Internet lines (x2)</li>-->
              </ul>
              <div class="price-box__btn">      
                <?php 
                  if (isset($_SESSION['usuario'])) {
                ?>
                  <a class="btn btn--purple btn--width" data-toggle="modal" data-target="#modalpedido" onclick="insertid(2)">Contrata ya</a>
                <?php 
                  }else{                  
                ?>
                  <a class="btn btn--purple btn--width" data-toggle="modal" data-target="#modalinformativa">Contrata ya</a>                  
                <?php 
                  }
                ?>
              </div>
            </div>
          </div>

          <!-- terzo -->
          <div class="col-md-4 col-sm-4 price-box price-box--blue">
            <div class="price-box__wrap">
              <div class="price-box__img"></div>
              <h1 class="price-box__title">
                Premium
              </h1>
              <p class="price-box__people">
                Baño, cocina y seguridad
              </p>
              <h2 class="price-box__discount">
              <span class="price-box__dollar">$</span>45,000<span class="price-box__discount--light"></span>
              </h2>
              <h3 class="price-box__price">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </h3>
              <p class="price-box__feat">
                Contiene:
              </p>
              <ul class="price-box__list">
                <li class="price-box__list-el">Módulo de seguridad</li>
                <li class="price-box__list-el">Módulo de cocina</li>
                <li class="price-box__list-el">Módulo de baño</li>
                <!--<li class="price-box__list-el"></li>
                <li class="price-box__list-el">HPE ProLiant DL380 Gen9 E5-2660v4 with Intel Xeon E5-2660 v4 processor, memory 1.0 TB with 512GB DDR4, 2x 500GB SSD Hard disk</li>-->
              </ul>
              <div class="price-box__btn">
                <?php 
                  if (isset($_SESSION['usuario'])) {
                ?>
                  <a class="btn btn--purple btn--width" data-toggle="modal" data-target="#modalpedido" onclick="insertid(3)">Contrata ya</a>
                <?php 
                  }else{                  
                ?>
                  <a class="btn btn--purple btn--width" data-toggle="modal" data-target="#modalinformativa">Contrata ya</a>                  
                <?php 
                  }
                ?>
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>

<!--
    <div class="sect sect--white" id="community">
      <div class="container">
        <div class="row">
          <h1 class="row__title">
            Join our Community
          </h1>
          
          <div class="sect sect--white sect--no-padding">
            <div class="container">
              <div class="row row--center">
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/mOtHRw/fblogo.png">
                  </a>
                </div>
                
                <div class="col-md-3  col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/nfpXRw/twitterlogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/imgOYb/googlelogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/ebGAeG/dribbblelogo.png">
                  </a>
                </div>
              </div>
              <div class="row row--center">
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/npV8Yb/gitlogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/cGyZ6w/stacklogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/ij03zG/inlogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/ekqdzG/codepenlogo.png">
                  </a>
                </div>
              </div>
            </div>    
          </div>
          
          <h2 class="row__sub">Community  <span class="row__sub--light">Road</span> Map</h2>
        </div>
        <div class="row row--margin row--text-center">
          <div class="col-md-8 col-sm-10 col-xs-12 row__carousel">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
            
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            
            <div class="carousel-inner">
              <div class="item active">
                <div class="item__content">
                  <img class="item__img" src="https://cdn.worldvectorlogo.com/logos/slack-1.svg" alt="Slack"><span class="item__name">slack</span>
                  <p class="item__description">
                    Q2 2018
                    * Assemble team and supporting personnel (completed) 
                    * Identify data center partner and cloud for co-hosting and security audit (completed) 
                    * Initiate infrastructure discussions (completed) 
                    * Form the shEOS partnership and legal entity (completed)  
                    * Set up technical infrastructure to be ready for EOS.IO launch 
                    * Host gatherings to help EOS token holders register wallets in preparation for the launch 
                    * Engage with blockchain educators to establish the shEOS scholarship initiative.
                  </p>
                </div>
                <div class="item__avatar"></div>
                <p class="item__people">Josh Doe</p>
                <p class="item__occupation">Ceo of Google</p>
              </div>
              
              <div class="item">
                <div class="item__content">
                  <img class="item__img" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Google-favicon-2015.png/150px-Google-favicon-2015.png" alt="Google"><span class="item__name">google</span>
                  <p class="item__description">
                    Q3 2018
                    * Fund our first class of shEOS scholars 
                    * Embark upon a speaking tour at large-scale events for the shEOSleadership team 
                    * Establish regional educational meet-ups 
                    * Support global EOS hackathons
                  </p>
                </div>
                <div class="item__avatar"></div>
                <p class="item__people">Mary Tompson</p>
                <p class="item__occupation">Ceo of Dribbble</a>
              </div>
              
              <div class="item">
                <div class="item__content">
                  <img class="item__img" src="https://www.hrexaminer.com/wp-content/uploads/2016/10/2016-10-11-hrexaminer-stackoverflow-6-xxl-sq-250px.png" alt="Stackoverflow"><span class="item__name">stackoverflow</span>
                  <p class="item__description">
                      Q4 2018
                      * Continue community engagement and expansion through meetups and innovative events with a focus on female involvement.
                  </p>
                </div>
                <div class="item__avatar"></div>
                <p class="item__people">Andrew Palmer</p>
                <p class="item__occupation">Ceo of Slack</p>
              </div>
              
              <div class="item">
                  <div class="item__content">
                    <img class="item__img" src="https://www.hrexaminer.com/wp-content/uploads/2016/10/2016-10-11-hrexaminer-stackoverflow-6-xxl-sq-250px.png" alt="Stackoverflow"><span class="item__name">stackoverflow</span>
                    <p class="item__description">
                        Q1 2019
                        * Develop educational content focused on attracting female developers to the EOS platform and to dissemination of information about the EOS platform.
                    </p>
                  </div>
                  <div class="item__avatar"></div>
                  <p class="item__people">Andrew Palmer</p>
                  <p class="item__occupation">Ceo of Slack</a>
                </div>
              </div>

              
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <img class="carousel-control__img" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjQ5NCAzMS40OTQiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjQ5NCAzMS40OTQ7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij4KPHBhdGggZD0iTTEwLjI3Myw1LjAwOWMwLjQ0NC0wLjQ0NCwxLjE0My0wLjQ0NCwxLjU4NywwYzAuNDI5LDAuNDI5LDAuNDI5LDEuMTQzLDAsMS41NzFsLTguMDQ3LDguMDQ3aDI2LjU1NCAgYzAuNjE5LDAsMS4xMjcsMC40OTIsMS4xMjcsMS4xMTFjMCwwLjYxOS0wLjUwOCwxLjEyNy0xLjEyNywxLjEyN0gzLjgxM2w4LjA0Nyw4LjAzMmMwLjQyOSwwLjQ0NCwwLjQyOSwxLjE1OSwwLDEuNTg3ICBjLTAuNDQ0LDAuNDQ0LTEuMTQzLDAuNDQ0LTEuNTg3LDBsLTkuOTUyLTkuOTUyYy0wLjQyOS0wLjQyOS0wLjQyOS0xLjE0MywwLTEuNTcxTDEwLjI3Myw1LjAwOXoiIGZpbGw9IiM2Zjc5ZmYiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" />
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <img class="carousel-control__img" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjQ5IDMxLjQ5IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzMS40OSAzMS40OTsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIzMnB4IiBoZWlnaHQ9IjMycHgiPgo8cGF0aCBkPSJNMjEuMjA1LDUuMDA3Yy0wLjQyOS0wLjQ0NC0xLjE0My0wLjQ0NC0xLjU4NywwYy0wLjQyOSwwLjQyOS0wLjQyOSwxLjE0MywwLDEuNTcxbDguMDQ3LDguMDQ3SDEuMTExICBDMC40OTIsMTQuNjI2LDAsMTUuMTE4LDAsMTUuNzM3YzAsMC42MTksMC40OTIsMS4xMjcsMS4xMTEsMS4xMjdoMjYuNTU0bC04LjA0Nyw4LjAzMmMtMC40MjksMC40NDQtMC40MjksMS4xNTksMCwxLjU4NyAgYzAuNDQ0LDAuNDQ0LDEuMTU5LDAuNDQ0LDEuNTg3LDBsOS45NTItOS45NTJjMC40NDQtMC40MjksMC40NDQtMS4xNDMsMC0xLjU3MUwyMS4yMDUsNS4wMDd6IiBmaWxsPSIjNmY3OWZmIi8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
              </a>
            </div>  
          </div>
        </div>
      </div>
    </div>

    <div class="sect sect--white sect--no-padding">
      <div class="container">
        <div class="row row--center">
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/mOtHRw/fblogo.png">
            </a>
          </div>
          
          <div class="col-md-3  col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/nfpXRw/twitterlogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/imgOYb/googlelogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/ebGAeG/dribbblelogo.png">
            </a>
          </div>
              
          
        </div>
        <div class="row row--center">
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/npV8Yb/gitlogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/cGyZ6w/stacklogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/ij03zG/inlogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/ekqdzG/codepenlogo.png">
            </a>
          </div>
        </div>
      </div>    
    </div>

    <div class="sect sect--white">
      <div class="container">
        <div class="row">
          <h1 class="row__title">
          Our blog
          </h1>
          <h2 class="row__sub">Sneak peeks from our writings</h2>
        </div>
        
        <div class="row row--margin">
          <div class="col-md-6 article-pre__col">
            <a href="#" class="article-pre ">
              <div class="article-pre__img article-pre__img--first"></div>
              <h2 class="article-pre__info">
                <span class="article-pre__cat">Protips • </span><span class="article-pre__aut"> by Ann Timothy</span> <span class="article-pre__date"> - 5 mins read</span>
              </h2>
              <h1 class="article-pre__title">How to improve analytics using few tools in Bricks<span class="article-pre__arrow--purple"> →</span></h1>
            </a>
          </div>
            <div class="col-md-6 article-pre__col">
              <a href="#" class="article-pre ">
                <div class="article-pre__img article-pre__img--second"></div>
                <h2 class="article-pre__info">
                  <span class="article-pre__cat">Pricing • </span><span class="article-pre__aut"> by Josh Ford</span> <span class="article-pre__date"> - 5 mins read</span>
                </h2>
                <h1 class="article-pre__title">Rich Thornett & Dan Coderholm about Dribbble in early 2009<span class="article-pre__arrow--purple">→</span></h1>
              </a>
            </div>    
          </div>
        <div class="row">
          <div class="col-md-6 article-pre__col">
            <a href="#" class="article-pre ">
              <div class="article-pre__img article-pre__img--fourth"></div>
              <h2 class="article-pre__info">
                <span class="article-pre__cat">Success Stories • </span><span class="article-pre__aut"> by Andrew Lincoln</span> <span class="article-pre__date"> - 5 mins read</span>
              </h2>
              <h1 class="article-pre__title">Steward Butterfield told us about his startup Slack<span class="article-pre__arrow--purple"> →</span></h1>
            </a>
          </div>
          <div class="col-md-6 article-pre__col">
            <a href="#" class="article-pre ">
              <div class="article-pre__img article-pre__img--third"></div>
              <h2 class="article-pre__info">
                <span class="article-pre__cat">Protips • </span><span class="article-pre__aut"> by Ann Timothy</span> <span class="article-pre__date"> - 5 mins read</span>
              </h2>
              <h1 class="article-pre__title">How to improve analytics using few tools in Bricks<span class="article-pre__arrow--purple"> →</span></h1>
            </a>
          </div>     
        </div>
      </div>
    </div>
  -->

    <div class="sect sect--padding-bottom" id="skechup">
      <div class="container">
        <div class="row">
          <h1 class="row__title text-primary">
            <b>Recreación</b>
          </h1>
          <h2 class="row__sub">Modelado en 3D</h2>
        </div>
        <div class="row row--margin">
          <div class="col-md-12 col-sm-12 price-box price-box--violet">
            <div class="price-box__wrap">
              <center>
                <iframe src="https://3dwarehouse.sketchup.com/embed.html?mid=1cb927fa-364a-4d34-afe2-c6055bd9e43c" 
                  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="580" 
                  height="326" allowfullscreen >
                </iframe> 
              </center>
              
              <p class="erroranchura">Para una optima experiencia en cuanto al modelado, se recomienda un dispositvo m&aacute;s grande  </p>            
              <div class="price-box__btn">
                <a class="btn btn--violet btn--width" data-toggle="tooltip" title="Presiona la imagen para ver modelo en 3D">Modelado</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="sect sect--padding-bottom" id="contactanos">
      <div class="container">
        <div class="row">
          <h1 class="row__title">
            Cont&aacute;ctanos
          </h1>
          <h2 class="row__sub">Si&eacute;ntanse libres de realizar cualquier pregunta.</h2>
        </div>
        <div class="row row--margin">
          <div class="col-md-1"></div>
          <div class="col-md-4">
            <div class="contacts">
              <a href="#" class="contacts__link"><img src="public/images/comq&1.jpg" width="80px"><h1 class="contacts_title-ag">Com<span class="contacts--light">Q&</span></h1></a>
              <p class="contacts__address"> 
                Carretera Federal M&eacute;xico - Pachuca, Km. 37.5<br>
                Predio Sierra Hermosa, C.P. 55740<br>
                Tec&aacute;mac, Estado de M&eacute;xico.
              </p>
              <p class="contacts__info">
                Tel. <a href="#" class="contacts__info-link">+52 5512345678</a>
              </p>
              <p class="contacts__info">
                Correo. <a href="#"class="contacts__info-link">info@comq.com</a>
              </p>
            </div>
          </div>
          <div class="col-md-6">
            <div id="contact" class="form">
              <div class="form-group">
                <select class="form__field form__select" required id="tipo">
                  <option value>Seleccione una*</option>
                  <option value="Informe" required>Solicitar informes</option>
                  <option value="Sugerencia" >Sugerencia</option>
                </select>
              </div>
              <div class="form-group">
                <div class="form__field--half">
                  <input type="text" placeholder="Nombre*" id="nombrecontacto" class="form__field form__text" required></input>
                </div>
                <div class="form__field--half">
                  <input type="text" placeholder="Apellidos*" id="apellidocontacto" class="form__field form__text" required></input>
                </div>
              </div>
              <div class="form-group">
                <div class="form__field--half">
                  <input type="text" placeholder="Correo electr&oacute;nico*" id="correocontacto" class="form__field form__text" required></input>
                </div>
                <div class="form__field--half">
                  <input type="number" placeholder="N&uacute;mero telef&oacute;nico" id="telcontacto" class="form__field form__text"></input>
                </div>
              </div>
              <div class="form-group">
                <textarea type="text" placeholder="Escriba su mensaje*" id="mensajecontacto" class="form__field form__textarea" required></textarea>
                <button class="btn btn--up btn--width" onclick="registrarcontacto()" >Enviar</button>
              </div>
              </div>
          </div>   
          <div class="col-md-1"></div>
        </div>
      </div>
    </div>

    <div class="sect sect--violet ">
      <img src="https://image.ibb.co/fWyVtb/path3762.png" class="career-img">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="career_title">ComQ&</h1>
            <h1 class="career_sub">Rápido, seguro y eficiente, como usted lo merece.</h1>
            <!--<a href="#" class="btn btn--white btn--width">Vote</a>-->
          </div>
        </div>
      </div> 
    </div>
    <!-- comienza footer -->
    <?php
    include("partials/_footer.php");
    ?>
    <!-- Modal informativa, inicia sesión-->
    <div id="modalinformativa" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Estimado Usuari@: </h4>
          </div>
          <div class="modal-body">
            <p>Para poder realizar la compra de alguno de nuestros servicios primero debe tener una cuenta en nuestro sitio web.</p>
            <p>Te invitamos a que visites el siguiente enlace para poder <a href="login.php">registrarse</a></p>
            <br><br>
            <p class="contacts__info">Nota: Solo se aceptan pagos via PayPal</p>
          </div>
          <div class="modal-footer">
            <a class="btn btn--default" data-dismiss="modal">Cerrar</a>        
          </div>
        </div>
      </div>
    </div>

    <!-- Modal dirección pedido -->
    <div id="modalpedido" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="row-title">Datos de direcci&oacute;n</h4>
          </div>          
          <div class="modal-body">
            <form action="pagopaypal.php" method="post">
              <input type="hidden" value="" id="idpaquete" name="idpaquete">            
                <input type="text" placeholder="Calle" name="calle" class="form-control form__field form__text" required><br>
                <input type="text" placeholder="Entre calles" name="calles" class="form-control form__field form__text" required><br>
                <input type="text" placeholder="Municipio" name="municipio" class="form-control form__field form__text" required><br>
                <!--<input type="text" placeholder="Estado" class="form-control form__field form__text" required>-->
                <select name="estado" class="form__field form__select" required>
                  <option value="">Selecciona una opci&oacute;n</option>
                  <option value="Aguascalientes">Aguascalientes</option>
                  <option value="Baja California">Baja California</option>
                  <option value="Baja California sur">Baja California Sur</option>
                  <option value="Campeche">Campeche</option>
                  <option value="Coahuila">Coahuila</option>
                  <option value="Colima">Colima</option>
                  <option value="Chiapas">Chiapas</option>
                  <option value="Chihuahua">Chihuahua</option>
                  <option value="Distrito Federal">Distrito Federal</option>
                  <option value="Durango">Durango</option>
                  <option value="Guanajuato">Guanajuato</option>
                  <option value="Guerrero">Guerrero</option>
                  <option value="Hidalgo">Hidalgo</option>
                  <option value="Jalisco">Jalisco</option>
                  <option value="México">México</option>
                  <option value="Michoacán">Michoacán</option>
                  <option value="Morelos">Morelos</option>
                  <option value="Nayarit">Nayarit</option>
                  <option value="Nuevo León">Nuevo León</option>
                  <option value="Oaxaca">Oaxaca</option>
                  <option value="Puebla">Puebla</option>
                  <option value="Querétaro">Querétaro</option>
                  <option value="Quintana Roo">Quintana Roo</option>
                  <option value="San Luis Potosí">San Luis Potosí</option>
                  <option value="Sinaloa">Sinaloa</option>
                  <option value="Sonora">Sonora</option>
                  <option value="Tabasco">Tabasco</option>
                  <option value="Tamaulipas">Tamaulipas</option>
                  <option value="Tlaxcala">Tlaxcala</option>
                  <option value="Veracruz">Veracruz</option>
                  <option value="Yucatán">Yucatán</option>
                  <option value="Zacatecas">Zacatecas</option>
                </select>                                
                <br><br>
                <input type="number" placeholder="Tel&eacute;fono de contacto" name="tel" class="form-control form__field form__text" required><br>                        
                <input type="number" placeholder="Codigo postal" name="cp" class="form-control form__field form__text" required>
          </div>
          <div class="modal-footer" border="none;">
            <center>
              <button class="btn btn--default" type="submit"> Proceder con el pago </button>
              </form>
              <a class="btn btn--default" data-dismiss="modal">Cancelar</a> 
            </center>            
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalseleccion" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Usuario: <?php echo $nombre; ?></h4>
          </div>
          <div class="modal-body">
            <a class="btn btn--default" data-toggle="modal" data-target="#myModal">Ver pedidos</a><br><br>
            <a class="btn btn--default" href="cerrarSesion.php">Cerrar sesi&oacute;n</a>          
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Usuario: <?php echo $nombre; ?></h4>
          </div>
          <?php
              $seleccionaColeccionCompra = $cliente -> selectCollection("isiukak","compra");
              //$query = $seleccionaColeccionPedido -> find(['nombre_c' => $nombre]);
              $query2 = $seleccionaColeccionCompra -> findOne(
              ['usuario' => new MongoDB\BSON\ObjectID($id_cliente)]                                 
              );
              $seleccionaColeccionPaquete = $cliente -> selectCollection("isiukak","paquete");
             /* $query3 = $seleccionaColeccionPaquete -> findOne(
              ['_id' => new MongoDB\BSON\ObjectID($query2['paquete'])]                                 
              );*/
              $query4 = $seleccionaColeccionCompra -> find(
                ['usuario' => new MongoDB\BSON\ObjectID($query2['usuario'])]);
              /*$query5 = $seleccionaColeccionPaquete -> find(
                  ['_id' => new MongoDB\BSON\ObjectID($query2['paquete'])]);*/
              //var_dump($query);
              //print_r($query);
              //$paquete = $query['paquete'];
              //$cantidad = $query['cantidad'];
              //$costo = $query['costo'];
              //$fecha = $query['fecha'];
              //print_r($query);
              //$pedidoArreglo = array($query['_id']);
              //print_r($pedidoArreglo);
              //$string = json_encode($pedidoArreglo);
              //$array = json_decode($string,true);
           ?>
          <div class="modal-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Pedido</th>
                <th>Cantidad</th> 
                <th>Costo</th>
                <th>Fecha</th>
              </tr>
            </thead>
            <tbody>
            <?php
            //print_r($array);
            //foreach ($query5 as $pedidoImprime) {
                foreach ($query4  as $pedido) {


                  $query5 = $seleccionaColeccionPaquete -> find(
                  ['_id' => new MongoDB\BSON\ObjectID($pedido['paquete'])]);
                  foreach ($query5 as $pedidoImprime) {
                  //$pedido = $pedido;
                    //$comment['paquete'];
                   // var_dump($comment);
                    //print_r($comment['paquete']);
                
                  
                  //var_dump($query);
                 // var_dump($pedido);
                  
                    //echo "Ahuevo que entra";
                    //$pedidoImprime = $pedidoImprime;
                    //var_dump($pedidoImprime);
                    //print_r($pedidoImprime);

             ?>         
              <tr class="info">
                <td><?php print_r($pedidoImprime['nombre']); ?></td>
                <td><?php print_r($pedido['cantidad']); ?></td>
                <td><?php print_r($pedido['costo']); ?></td>
                <td><?php print_r($pedido['fecha']); ?></td>
              </tr>  
              <?php 
                  }
                  unset($pedidoImprime);
                }
                unset($pedido);
               ?>                         
            </tbody>
          </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>  
  </body>
  <script src="public/style/transicion.js"></script>
  <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });
  </script>
  <script src="public/ajax/contacto.js"></script>
</html>