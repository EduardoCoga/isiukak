<?php 
    include("conexionMongo.php");
    session_start();
    if (isset($_POST['idpaquete']) && isset($_SESSION['usuario'])){
        //echo $_GET['id'];
        $id=$_POST['idpaquete'];
        $calle=$_POST['calle'];
        $entrecalles=$_POST['calles'];
        $municipio=$_POST['municipio'];
        $estado=$_POST['estado'];
        $cp=$_POST['cp'];
        $tel=$_POST['tel'];
        $seleccionaColeccion = $cliente -> selectCollection("isiukak","paquete");
        $query = $seleccionaColeccion -> findOne(array('id' => $id));
        $fecha = getdate();$fecinsert=$fecha['year'].'-'.$fecha['mon'].'-'.$fecha['mday'];$date = new DateTime($fecinsert);$f = $date->format('Y-m-d');
        $idpaquete=$query['_id'];
        $idusuario=$_SESSION['_id'];
        $costo=$query['costo'];
        //echo $f;
        $seleccionaColeccion2 = $cliente -> selectCollection("isiukak","compra");
        $query2 = $seleccionaColeccion2 -> insertOne([
            'paquete'=>$idpaquete,
            'usuario'=>$idusuario,
            'costo'=> $costo,
            'cantidad'=> 1,
            'fecha'=> $f,
            'telContacto' => $tel,
            'direccion' => $calle.' entre calles '.$entrecalles.' del municipio '.$municipio.' del Edo. de '.$estado.', <b>'.$cp.'</b>',
            'idtransaccion' => '',
            'estatus' => false
            ]);
        $seleccionaColeccion3 = $cliente -> selectCollection("isiukak","compra");
        $query3 = $seleccionaColeccion3 -> findOne([],['sort' => ['_id' => -1]]);  
        //echo $query3['_id'];   
        $com=$query3['_id'];                  
        $product_name = $query['nombre'];//$result['product_name'];//Nombre del producto
        $product_price = $query['costo'];//$result['product_price'];//Precio del producto
        $product_currency = 'MXN';//$result['product_currency'];//Moneda del producto     
        //URL Paypal Modo pruebas.
        $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        //URL Paypal para Recibir pagos 
        //$paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
        //Correo electronico del comercio. 
        $merchant_email = 'silviayasociadoss.a@gmail.com';
        //Pon aqui la URL para redireccionar cuando el pago es completado
        $cancel_return = "http://localhost:8080/m/isiukak/paypalinfo.php?st=false";
        //Colocal la URL donde se redicciona cuando el pago fue completado con exito.
        $success_return = "http://localhost:8080/m/isiukak/paypalinfo.php?st=true&pa=$id&comp=$com";
?>
    <div style="margin-top: 18%">
        <center><img src="./public/images/cargando.gif"/></center>
    </div>
    <form name="myform" action="<?php echo $paypal_url; ?>" method="post" target="_top">
        <input type="hidden" name="business" value="<?php echo $merchant_email; ?>">
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="cancel_return" value="<?php echo $cancel_return ?>">
        <input type="hidden" name="return" value="<?php echo $success_return; ?>">

        <input type="hidden" name="handling" value="0">
        <input type="hidden" name="no_shipping" value="1">


        <input type="hidden" name="item_name" value="<?php echo $product_name; ?>">
        <input type="hidden" name="item_number" value="<?php echo $id; ?>">
        <input type="hidden" name="amount" value="<?php echo $product_price; ?>">
        <input type="hidden" name="currency_code" value="<?php echo $product_currency; ?>">
        <!--<input type="hidden" name="lc" value="C2">
        <input type="hidden" name="button_subtype" value="services">
        <input type="hidden" name="no_note" value="0">-->
    </form>
    <script type="text/javascript">
        document.myform.submit();
    </script>
<?php		
} else {
	header("location:index.php");
	exit;
}
?>