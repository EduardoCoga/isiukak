function registrarcontacto () {
    //console.log($('#telcontacto').val().length)
    if ($('#telcontacto').val().length==10) {
        $.ajax({
            data:{
                tipo:$('#tipo').val(),
                nombre:$('#nombrecontacto').val(),
                apellidos:$('#apellidocontacto').val(),
                correo:$('#correocontacto').val(),
                telefono:$('#telcontacto').val(),
                mensaje:$('#mensajecontacto').val()
            },
            url:'contactanos.php',
            type:'POST',
            beforeSend: function(){
                swal({
                    title: '<strong> Espere un momento.</strong>',
                    type: 'info',
                    html:
                      '...',
                    showCloseButton: false,
                    showCancelButton: false,
                    showConfirmButton:false
                  })
            },
            success:function(response){                                             
                swal(
                    '¡Exito!',
                    "Se envio su solicitud de "+$('#tipo').val()+" de manera correcta",
                    'success'
                );
                $('#nombrecontacto').val('');$('#apellidocontacto').val('');$('#correocontacto').val('');
                $('#telcontacto').val('');$('#mensajecontacto').val('');$('#tipo').val('');
            }
        });
    }else{
        swal(
            '¡Error!',
            "El número telefónico debe contener 10 digitos",
            'error'
        )  
    }
}

//
function insertid(value){
    $("#idpaquete").attr("value",value);
}