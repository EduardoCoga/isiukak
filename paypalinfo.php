<?php 
    include("conexionMongo.php");
    session_start();
    //echo($_SESSION['usuario']);
    if (!isset($_SESSION['usuario'])) {
        header("location: index.php");
    }                    
 ?>
<html>
    <style>        
        .sect2 {
            padding: 0 0;
            position: relative;
        }
        @media screen and (max-width: 1024px) {
            .sect2 {
            padding: 110px 0;
            position: relative;
            }
        }
        @media screen and (max-width: 500px) {
            .sect2 {
            padding: 110px 0;
            position: relative;
            }
        }
    </style>
    <head>
        <?php
        include("partials/_head.php");
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
    <!-- comienza header -->
    <header class="header">
        <div class="container header__container">
            <div class="header__logo"><img class="header__img" src="public/images/comq&.png" width="20%">
            </div> 
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <div class="header__menu">
                <nav id="navbar" class="header__nav collapse">
                    <ul class="header__elenco">
                        <center><li class="header__el"><a href="./index.php" class="header__link">Inicio</a></li></center>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!-- termina header -->
    

    <div class="sect2 sect--padding-bottom" id="servicios">
        <div class="container">
            <div class="row row--center row--margin">
                <div class="col-md-12 col-sm-12 price-box ">
                    <!--Exito-->
                    <?php if($_GET['st'] == true && $_GET['pa']!='' && $_GET['comp']!='' ) { ?>
                        <?php if($_GET['pa'] == 1 || $_GET['pa'] == 2 || $_GET['pa'] == 3) { 
                            include("conexionMongo.php");
                            $idcom=$_GET['comp'];
                            $cad = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 15);
                            $seleccionaColeccion = $cliente -> selectCollection("isiukak","compra");
                            $query = $seleccionaColeccion -> updateOne(
                                ['_id' => new MongoDB\BSON\ObjectID($idcom)], 
                                ['$set' => [ 'estatus' => true,
                                            'idtransaccion'=>'PAY-'.$cad]]                                
                            );                            
                            $seleccionaColeccion2 = $cliente -> selectCollection("isiukak","compra");
                            $query2 = $seleccionaColeccion2 -> findOne(
                                ['_id' => new MongoDB\BSON\ObjectID($idcom)]                                 
                            );   
                            $seleccionaColeccion3 = $cliente -> selectCollection("isiukak","paquete");
                            $query3 = $seleccionaColeccion3 -> findOne(
                                ['_id' => new MongoDB\BSON\ObjectID($query2['paquete'])]                                 
                            ); 
                            //echo $query3['nombre'];
                            //print_r($query);                            
                            ?>
                            <div class="price-box__wrap">                        
                                    <img src="./public/images/pago.png" width="130px" height="130px">                        
                                <h1 class="price-box__title bg-success text-white">
                                    Transacci&oacute;n exitosa
                                </h1>
                                <p class="price-box__people">
                                    <b>Paquete: <?php echo $query3['nombre']; ?></b>
                                </p>
                                <h2 class="price-box__discount">
                                    <span class="price-box__dollar"></span><?php echo $query2['costo']; ?><span class="price-box__discount--light"></span>
                                </h2>
                                <h3 class="price-box__price">
                                    Módulo de seguridad desde el cual se puede monitorear ingresos y egresos a un establecimiento específico.
                                </h3>
                                <p class="price-box__feat">
                                    Detalles de la transacci&oacute;n:
                                </p>
                                <ul class="price-box__list">
                                    <li class="price-box__list-el"><b>ID. transacci&oacute;n:</b> PAY - <?php echo $cad; ?></li>
                                    <li class="price-box__list-el"><b>Fecha:</b> <?php echo $query2['fecha'];?></li>
                                    <li class="price-box__list-el"><b>Direcci&oacute;n de instalaci&oacute;n:</b> <?php echo $query2['direccion']; ?></li>
                                    <!--<li class="price-box__list-el">Q4 2018 - Additional Node in South America</li>-->
                                </ul>
                                <p class="price-box__feat">
                                    <a type="button" class="btnsuc btn-success" href="./index.php">Ir a inicio</a>
                                </p> 
                                <div class="price-box__btn">
                                </div>                                
                            </div>
                        <?php } else {?>    
                            <div class="price-box__wrap">                        
                                <img src="./public/images/error.png" width="130px" height="130px">                        
                            <h1 class="price-box__title text-danger">
                                Ocurrio un error durante la transacci&oacute;n
                            </h1>                        
                            <p class="price-box__feat">
                                <a type="button" class="btnerr btn-danger" href="./index.php">Ir a inicio</a>
                            </p>                        
                            <div class="price-box__btn">
                            </div>
                        </div>
                        <?php } ?>
                    <?php } else { ?>                    
                    <!--Error-->
                        <div class="price-box__wrap">                        
                                <img src="./public/images/error.png" width="130px" height="130px">                        
                            <h1 class="price-box__title text-danger">
                                Ocurrio un error durante la transacci&oacute;n
                            </h1>                        
                            <p class="price-box__feat">
                                <a type="button" class="btnerr btn-danger" href="./index.php">Ir a inicio</a>
                            </p>                        
                            <div class="price-box__btn">
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>


    <div class="sect sect--violet ">
        <img src="https://image.ibb.co/fWyVtb/path3762.png" class="career-img">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="career_title">ComQ&</h1>
                    <h1 class="career_sub">Rápido, seguro y eficiente, como usted lo merece.</h1>
                    <!--<a href="#" class="btn btn--white btn--width">Vote</a>-->
                </div>
            </div>
        </div> 
    </div>
    <!-- comienza footer -->
    <?php
    include("partials/_footer.php");
    ?>
  </body>
</html>